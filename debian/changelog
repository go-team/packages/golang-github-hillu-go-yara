golang-github-hillu-go-yara (4.3.3-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Sat, 03 Aug 2024 17:03:34 +0200

golang-github-hillu-go-yara (4.3.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump YARA version requirement in d/control.
  * Bump Standards-Version.

 -- Sascha Steinbiss <satta@debian.org>  Thu, 15 Jun 2023 10:31:03 +0200

golang-github-hillu-go-yara (4.2.4-2) unstable; urgency=medium

  * Fix watchfile by adjusting line ending.

 -- Sascha Steinbiss <satta@debian.org>  Sat, 08 Oct 2022 16:04:49 +0200

golang-github-hillu-go-yara (4.2.4-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 19 Aug 2022 13:33:04 +0200

golang-github-hillu-go-yara (4.2.3-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 13 May 2022 10:13:33 +0200

golang-github-hillu-go-yara (4.2.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump YARA version requirement in d/control.

 -- Sascha Steinbiss <satta@debian.org>  Tue, 03 May 2022 07:39:57 +0200

golang-github-hillu-go-yara (4.2.1-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Sat, 16 Apr 2022 11:53:17 +0200

golang-github-hillu-go-yara (4.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump YARA version requirement in d/control.

 -- Sascha Steinbiss <satta@debian.org>  Sat, 26 Mar 2022 22:50:34 +0100

golang-github-hillu-go-yara (4.1.0-3) unstable; urgency=medium

  * Fix d/watch.

 -- Sascha Steinbiss <satta@debian.org>  Thu, 17 Feb 2022 20:31:57 +0100

golang-github-hillu-go-yara (4.1.0-2) unstable; urgency=medium

  * Upload to unstable post-release.

 -- Sascha Steinbiss <satta@debian.org>  Tue, 17 Aug 2021 11:42:01 +0200

golang-github-hillu-go-yara (4.1.0-1) experimental; urgency=medium

  * New upstream release.
  * Require YARA 4.1 or later.
  * Bump Standards-Version.

 -- Sascha Steinbiss <satta@debian.org>  Mon, 07 Jun 2021 14:02:14 +0200

golang-github-hillu-go-yara (4.0.4-1) unstable; urgency=medium

  * New upstream release.
  * Add Rules-Requires-Root.
  * Use watchfile version 4.

 -- Sascha Steinbiss <satta@debian.org>  Thu, 31 Dec 2020 12:16:09 +0100

golang-github-hillu-go-yara (4.0.3-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Tue, 17 Nov 2020 10:09:52 +0100

golang-github-hillu-go-yara (4.0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Tue, 30 Jun 2020 10:43:28 +0200

golang-github-hillu-go-yara (4.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump libyara build-dep to 4.0 or later.
  * Add runtime dependency on libyara.
  * Remove unnecessary shlibs:Depends macro.
  * Use debhelper 13.
  * Update date in d/copyright.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 05 Jun 2020 09:30:41 +0200

golang-github-hillu-go-yara (1.2.2-1) unstable; urgency=medium

  * New upstream release.
  * Use debhelper-compat 12.
  * Bump Standards-Version.

 -- Sascha Steinbiss <satta@debian.org>  Sat, 18 Apr 2020 18:46:22 +0200

golang-github-hillu-go-yara (1.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version.

 -- Sascha Steinbiss <satta@debian.org>  Tue, 13 Nov 2018 14:38:53 +0100

golang-github-hillu-go-yara (1.0.9-1) unstable; urgency=medium

  * New upstream release.
  * Use debhelper 11.
  * Bump Standards-Version.

 -- Sascha Steinbiss <satta@debian.org>  Sat, 14 Jul 2018 18:02:56 +0200

golang-github-hillu-go-yara (1.0.8-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Thu, 15 Mar 2018 22:44:15 +0100

golang-github-hillu-go-yara (1.0.7-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 16 Feb 2018 16:02:54 +0100

golang-github-hillu-go-yara (1.0.6-2) unstable; urgency=medium

  * Team upload.
  * Vcs-* urls: pkg-go-team -> go-team.

 -- Alexandre Viau <aviau@debian.org>  Tue, 06 Feb 2018 00:31:22 -0500

golang-github-hillu-go-yara (1.0.6-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version.
  * Set correct Vcs-* entries for Salsa.

 -- Sascha Steinbiss <satta@debian.org>  Tue, 30 Jan 2018 11:57:35 +0100

golang-github-hillu-go-yara (1.0.5-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version.
  * Add autopkgtest.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 08 Dec 2017 17:50:44 +0000

golang-github-hillu-go-yara (1.0.4-1) unstable; urgency=medium

  * New upstream release.
  * Adjust d/copyright dates.
  * Add new dependency on pkg-config.
  * Bump Standards-Version.
  * Change priority to optional to comply with 4.0.1.

 -- Sascha Steinbiss <satta@debian.org>  Fri, 27 Oct 2017 11:03:23 +0200

golang-github-hillu-go-yara (1.0.3-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version.

 -- Sascha Steinbiss <satta@debian.org>  Sun, 25 Jun 2017 00:27:46 +0200

golang-github-hillu-go-yara (1.0.2-1) unstable; urgency=medium

  * New upstream release.

 -- Sascha Steinbiss <satta@debian.org>  Sun, 09 Apr 2017 07:55:40 +0000

golang-github-hillu-go-yara (1.0.0-1) unstable; urgency=medium

  * Initial release (Closes: #858347)

 -- Sascha Steinbiss <satta@debian.org>  Tue, 21 Mar 2017 15:30:10 +0100
